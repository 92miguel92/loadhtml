//
//  Connection.swift
//  LectorHtml
//
//  Created by Master Móviles on 17/01/2017.
//  Copyright © 2017 Antonio Pertusa. All rights reserved.
//

import Foundation

// Protocolo de Connection
protocol ConnectionDelegate {
    func connectionSucceed(_ connection : Connection, with data: Data)
    func connectionFailed(_ connection: Connection, with error: String)
}

class Connection {
    
    // Delegado
    var delegate:ConnectionDelegate?
    
    // Función de inicialización, necesita la clase delegada
    init(delegate : ConnectionDelegate) {
        self.delegate = delegate
    }
    
    // Función para lanzar una petición con un URLRequest
    func startConnection(_ session: URLSession, with request:URLRequest) {
        
        session.dataTask(with: request, completionHandler: { data, response, error in
            
            if let error = error {
                self.delegate?.connectionFailed(self, with: error.localizedDescription)
            }
            else {
                let res = response as! HTTPURLResponse
                
                if res.statusCode == 200 {
                    DispatchQueue.main.async {
                        self.delegate?.connectionSucceed(self, with: data!)
                    }
                }
                else {
                    let errorMessage=("Received status code: \(res.statusCode)")
                    self.delegate?.connectionFailed(self, with: errorMessage)
                }
            }
        }).resume()
    }
    
    // Función para lanzar una petición con una URL
    func startConnection(_ session: URLSession, with url:URL) {
        self.startConnection(session, with: URLRequest(url:url))
    }
    
    // Función para lanzar la petición con un String
    func startConnection(_ session: URLSession, with urlString:String) {
        if let encodedString = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) {
            if let url = URL(string: encodedString) {
                self.startConnection(session, with: url)
            }
        }
    }
}
